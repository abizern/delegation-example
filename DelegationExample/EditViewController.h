//
//  EditViewController.h
//  DelegationExample
//
//  Created by Abizer Nasir on 04/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditViewControllerDelegate <NSObject>

@optional
- (void)updateText:(NSString *)text;

@end

@interface EditViewController : UIViewController

@property (weak, nonatomic) id<EditViewControllerDelegate> delegate;
@property (copy, nonatomic) NSString *editText;
@property (weak, nonatomic) IBOutlet UITextField *editLabel;

// The background is a UIControl instance not a UIView.
// When touched it sends this action, and the label
// can relinquish first responder status.
- (IBAction)backgroundTouched:(id)sender;

@end
