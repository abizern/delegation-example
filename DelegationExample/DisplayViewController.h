//
//  DisplayViewController.h
//  DelegationExample
//
//  Created by Abizer Nasir on 04/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditViewController.h"
#import "SegueNames.h"

@interface DisplayViewController : UIViewController <EditViewControllerDelegate>

@property (copy, nonatomic) NSString *displayText;
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;

@end
