//
//  DisplayViewController.m
//  DelegationExample
//
//  Created by Abizer Nasir on 04/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import "DisplayViewController.h"

static NSString * const kDefaultDisplayText = @"Default Text";

@interface DisplayViewController ()

@end

@implementation DisplayViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (self.displayText) {
        self.displayLabel.text = self.displayText;
    } else {
        self.displayLabel.text = kDefaultDisplayText;
    }
}

#pragma mark - Delegate methods

- (void)updateText:(NSString *)text {
    self.displayText = text;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (![segue.identifier isEqualToString:kDisplayToEditSegue]) {
        return;
    }

    EditViewController *destinationController = segue.destinationViewController;
    destinationController.delegate = self;
    destinationController.editText = self.displayText;
}

@end
