//
//  SegueNames.h
//  DelegationExample
//
//  Created by Abizer Nasir on 04/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//
//  A convenience header file for storing the segue names as static strings

static NSString * const kDisplayToEditSegue = @"EditTextSegue";
