//
//  EditViewController.m
//  DelegationExample
//
//  Created by Abizer Nasir on 04/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()

@end

@implementation EditViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.editLabel.text = self.editText;
}

- (void)viewWillDisappear:(BOOL)animated {
    // Everytime the view disappears - update the text in the display controller.
    if ([self.delegate respondsToSelector:@selector(updateText:)]) {
        [self.delegate updateText:self.editLabel.text];
    }
}

- (IBAction)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
}

@end
