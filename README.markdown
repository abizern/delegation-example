## DelegationExample

A common question for new iOS developers - how do I pass information back to a
controller lower in the stack.

This simple application provides an example of how to pass information up the
stack in the segue and down the stack using delegates.

### Dependencies

- iOS6
- Uses Storyboards and ARC

### Issues / requests

Please create an issue on this repo and I'll take a look.
